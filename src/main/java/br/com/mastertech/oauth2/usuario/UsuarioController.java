package br.com.mastertech.oauth2.usuario;

import br.com.mastertech.oauth2.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @GetMapping("/{usuario}")
    public Usuario create(@AuthenticationPrincipal Usuario usuario) {
        return usuario;
    }
}
